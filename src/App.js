import React, { Component } from 'react';

import { Tokbox } from './chat/Tokbox';

export class App extends Component {

  render() {

    return (
      <div>
        <Tokbox />
      </div>
    );
  }
}
