import React, { Component } from 'react';
import Peer from 'simple-peer';

export class Girl extends Component {

  constructor (props) {
    super(props);

    this.state = { chat: '',
                   girlReady: false,
                   manReady: false,
                   transReady: false
                  };

    this.girlReady = () => {
      console.log('girlReady');
      socketIO.emit('ready', 'girl');
    };

    socketIO.on('ready', (msg) => {
      console.log('ready', msg);
      if (msg == 'man') {
        this.setState({manReady: true});
      } else if (msg == 'girl') {
        this.setState({girlReady: true});
      } else if (msg == 'trans') {
        this.setState({transReady: true});
      }
    });

    this.checkReady = () => {
      var {manReady, girlReady, transReady} = this.state;
      return manReady && girlReady && transReady;
    };

  }

  componentDidUpdate () {
    if (this.checkReady()) {
      socketIO.emit('signal', JSON.stringify(this.state.offer2trans));
    }
  }

  componentDidMount () {
    var girl2man, girl2trans; // peers

    navigator.getUserMedia( {video: true }, (stream) => {

      girl2man = new Peer({ trickle: false, stream: stream});
      girl2trans = new Peer({ initiator: true, trickle: false, stream: stream });

      // initiator (create offer)
      girl2trans.on('signal', (data) => {
        var offer2trans = {};
        offer2trans.data = data;
        offer2trans.from = 'girl2trans';
        this.setState({offer2trans});
        // socketIO.emit('signal', JSON.stringify(offer2trans));
      });

      // response to initiators (create answer);
      girl2man.on('signal', (data) => {
        var answer2man = {};
        answer2man.data = data;
        answer2man.from = 'girl2man';
        socketIO.emit('signal', JSON.stringify(answer2man));
      });

      // handle sockets signals
      socketIO.on('signal', (msg) => {
        var signal = JSON.parse(msg);
        if (signal.from == 'man2girl') {
          girl2man.signal(signal.data);
        }
        if (signal.from == 'trans2girl') {
          girl2trans.signal(signal.data);
        }
      });

      // handle Streams
      girl2man.on('stream', (stream) => {
        var video = document.querySelector('video');
        video.src = window.URL.createObjectURL(stream);
        video.play();
      });

    }, () => {} );
  }

  render () {
    return (
      <div>
        <div id="girlVideoContainer">girlVideoContainer</div>
        <div id="manVideoContainer">
          <video></video>
        </div>
        <button onClick={this.girlReady}
                className="waves-effect waves-light btn">Girl Ready</button>
        <div dangerouslySetInnerHTML={{__html: this.state.chat}}></div>
      </div>
    );
  }

}
