import React, { Component } from 'react';
import Peer from 'simple-peer';

export class Man extends Component {

  constructor (props) {
    super(props);

    this.state = { chat: '',
                   girlReady: false,
                   manReady: false,
                   transReady: false
                 };

    // test socket.io //
    socketIO.on('chat message', (msg) => {
      console.log(msg);
    });

    this.signal = () => {
      socketIO.emit('chat message', 'man message');
    };

    this.manReady = () => {
      console.log('manReady');
      socketIO.emit('ready', 'man');
    };

    socketIO.on('ready', (msg) => {
      console.log('ready', msg);
      if (msg == 'man') {
        this.setState({manReady: true});
      } else if (msg == 'girl') {
        this.setState({girlReady: true});
      } else if (msg == 'trans') {
        this.setState({transReady: true});
      }
    });

    this.checkReady = () => {
      var {manReady, girlReady, transReady} = this.state;
      return manReady && girlReady && transReady;
    };

  }

  componentDidUpdate () {
    if (this.checkReady()) {
      socketIO.emit('signal', JSON.stringify(this.state.offer2girl));
      socketIO.emit('signal', JSON.stringify(this.state.offer2trans));
    }
  }

  componentDidMount () {

    var man2girl, man2trans; // peers

    navigator.getUserMedia( {video: true }, (stream) => {

      man2girl = new Peer({ initiator: true, trickle: false, stream: stream });
      man2trans = new Peer({ initiator: true, trickle: false, stream: stream });

      // initiator (create offer)
      man2girl.on('signal', (data) => {
        var offer2girl = {};
        offer2girl.data = data;
        offer2girl.from = 'man2girl';
        this.setState({offer2girl});
        // socketIO.emit('signal', JSON.stringify(offer2girl));
      });
      man2trans.on('signal', (data) => {
        var offer2trans = {};
        offer2trans.data = data;
        offer2trans.from = 'man2trans';
        this.setState({offer2trans});
        // socketIO.emit('signal', JSON.stringify(offer2trans));
      });

      // response to initiators (create answer);

      // handle sockets signals
      socketIO.on('signal', (msg) => {
        var signal = JSON.parse(msg);
        if (signal.from == 'girl2man') {
          man2girl.signal(signal.data);
        }
        if (signal.from == 'trans2man') {
          man2trans.signal(signal.data);
        }
      });

      // hangle Streams
      man2girl.on('stream', (stream) => {
        var video = document.querySelector('video');
        video.src = window.URL.createObjectURL(stream);
        video.play();
      });

    }, () => {} );

  }

  render () {
    return (
      <div>
        <div id="manVideoContainer">manVideoContainer</div>
        <div id="girlVideoContainer">
          <video></video>
        </div>
        <button onClick={this.manReady}
                className="waves-effect waves-light btn">Man Ready</button>
        <div dangerouslySetInnerHTML={{__html: this.state.chat}}></div>
        <input ref="message"></input>
        <button onClick={this.signal}
                className="waves-effect waves-light btn">Send Message</button>
      </div>
    );
  }
}
