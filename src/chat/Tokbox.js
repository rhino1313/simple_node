import React, { Component } from 'react';

import { Man } from './Man';
import { Girl } from './Girl';
import { Trans } from './Trans';

export class Tokbox extends Component {

  constructor (props) {
    super(props);

    this.state = { man: false, girl: false, trans: false };

    this.setMan = () => {
      this.setState({ man: true, girl: false, trans: false });
    };
    this.setGirl = () => {
      this.setState({ man: false, girl: true, trans: false });
    };
    this.setTrans = () => {
      this.setState({ man: false, girl: false, trans: true });
    };
  }

  componentDidMount () {

  }

  render () {
    return (
      <div>
        <button onClick={this.setMan}
                className="waves-effect waves-light btn">Man</button>
        <button onClick={this.setGirl}
                className="waves-effect waves-light btn">Girl</button>
        <button onClick={this.setTrans}
                className="waves-effect waves-light btn">Translator</button>
        {this.state.man && <Man />}
        {this.state.girl && <Girl />}
        {this.state.trans && <Trans />}
      </div>
    );
  }
}
