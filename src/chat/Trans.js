import React, { Component } from 'react';
import Peer from 'simple-peer';

export class Trans extends Component {

  constructor (props) {
    super(props);

    this.state = { chat: '',
                   girlReady: false,
                   manReady: false,
                   transReady: false
                  };

    this.signal = () => {
      console.log('signal');
    };

    this.transReady = () => {
      console.log('transReady');
      socketIO.emit('ready', 'trans');
    };

    socketIO.on('ready', (msg) => {
      console.log('ready', msg);
      if (msg == 'man') {
        this.setState({manReady: true});
      } else if (msg == 'girl') {
        this.setState({girlReady: true});
      } else if (msg == 'trans') {
        this.setState({transReady: true});
      }
    });

    this.checkReady = () => {
      var {manReady, girlReady, transReady} = this.state;
      return manReady && girlReady && transReady;
    };

  }

  componentDidMount () {
    var trans2man, trans2girl; // peers

    navigator.getUserMedia( {video: true }, (stream) => {

      trans2man = new Peer({ trickle: false /* , stream: stream */ });
      trans2girl = new Peer({ trickle: false /* , stream: stream */ });

      // initiator (create offer)

      // response to initiators (create answer);
      trans2man.on('signal', (data) => {
        var answer2man = {};
        answer2man.data = data;
        answer2man.from = 'trans2man';
        socketIO.emit('signal', JSON.stringify(answer2man));
      });
      trans2girl.on('signal', (data) => {
        var answer2girl = {};
        answer2girl.data = data;
        answer2girl.from = 'trans2girl';
        socketIO.emit('signal', JSON.stringify(answer2girl));
      });

      // handle sockets signals
      socketIO.on('signal', (msg) => {
        var signal = JSON.parse(msg);
        if (signal.from == 'man2trans') {
          trans2man.signal(signal.data);
        }
        if (signal.from == 'girl2trans') {
          trans2girl.signal(signal.data);
        }
      });

      // handle Streams
      trans2man.on('stream', (stream) => {
        var video = document.querySelector('#video-man');
        video.src = window.URL.createObjectURL(stream);
        video.play();
      });
      trans2girl.on('stream', (stream) => {
        var video = document.querySelector('#video-girl');
        video.src = window.URL.createObjectURL(stream);
        video.play();
      });

    }, () => {} );
  }

  render () {
    return (
      <div>
        <div id="girlVideoContainer">
          <video id="video-girl"></video>
        </div>
        <div id="manVideoContainer">
          <video id="video-man"></video>
        </div>
        <button onClick={this.transReady}
                className="waves-effect waves-light btn">Trans Ready</button>
        <div dangerouslySetInnerHTML={{__html: this.state.chat}}></div>
        <input ref="message"></input>
        <button onClick={this.signal}
                className="waves-effect waves-light btn">Send Message</button>
      </div>
    );
  }
}
