import React from 'react';
import { App } from './App';

navigator.getUserMedia = navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia ||
                         navigator.msGetUserMedia;

React.render(<App />, document.getElementById('root'));
