var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use('/static', express.static(__dirname + '/public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  console.log('new connection');

  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });

  socket.on('signal', function(msg){
    console.log('signal');
    io.emit('signal', msg);
  });

  socket.on('ready', function(msg) {
    console.log('ready', msg);
    io.emit('ready', msg);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
